require 'spec_helper'

describe ImageTransformationService do
  describe '#call' do
    context 'invalid format' do
      it 'raise error' do
        [{}, {file: ''}].each do |params|
          expect { described_class.new(params).call }.to raise_error(ImageTransformationService::ImageNotFoundError)
        end
      end
    end

    context 'valid format' do
      let!(:image) { FactoryBot.create(:image) }
      let(:params) { {id: image.id, specs: {format: 'png'}} }

      it 'creates a new transformation' do
        expect {
          described_class.new(params).call
        }.to change { Image.count }.by(1)
      end
    end
  end
end