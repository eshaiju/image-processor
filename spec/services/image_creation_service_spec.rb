require 'spec_helper'

describe ImageCreationService do
  describe '#call' do
    context 'invalid format' do
      it 'raise error :InvalidFileFormat' do
        [{}, {file: ''}].each do |params|
          expect { described_class.new(params).call }.to raise_error(ImageCreationService::InvalidFileFormat)
        end
      end
    end

    context 'valid format' do
      context 'remote file' do
        let(:params) { {file: 'https://rubyonjets.com/img/logos/jets-logo-full.png'} }

        it 'creates a new Image' do
          expect {
            described_class.new(params).call
          }.to change { Image.count }.by(1)
        end
      end

      context 'local file' do
        let(:params) { {file: File.open('spec/fixtures/images/jets-logo-full.png')} }

        it 'creates a new Image' do
          expect {
            described_class.new(params).call
          }.to change { Image.count }.by(1)
        end
      end
    end
  end
end