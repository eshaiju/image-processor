FactoryBot.define do
  factory :image do
    file { Rack::Test::UploadedFile.new(Jets.root.join('spec/fixtures/images/jets-logo-full.png'), 'image/png') }
  end
end