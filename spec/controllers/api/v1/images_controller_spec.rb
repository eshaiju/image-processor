require 'spec_helper'
require 'rack/test'

describe Api::V1::ImagesController do
  describe '#show' do
    let(:image) { FactoryBot.create(:image) }

    context 'when an Image is exist' do
      before(:each) do
        get '/api/v1/images/:id', id: image.id
      end

      it 'returns image details' do
        expect(json_response).to eq(
                                     {
                                         "data": {
                                             "id": image.id,
                                             "type": "image",
                                             "attributes": {
                                                 "id": image.id,
                                                 "file_url": image.file_url,
                                                 "transformations": []
                                             }
                                         }
                                     }
                                 )
      end

      it 'should respond with a status code 200' do
        expect(response.status).to eq(200)
      end
    end

    context 'when an Image not exist' do
      it 'returns a 404' do
        get '/api/v1/images/:id', id: '222'

        expect(response.status).to eq(404)
      end
    end
  end

  describe '#create' do
    describe 'with valid params' do
      let(:file) { 'https://rubyonjets.com/img/logos/jets-logo-full.png' }

      context 'with valid params' do
        before(:each) do
          post '/api/v1/images', params: {file: file}
        end

        it 'should create a new image' do
          latest_image = Image.order(created_at: :desc).first
          expect(json_response[:data][:attributes]).to eql(
                                                           id: latest_image.id,
                                                           file_url: latest_image.file_url,
                                                           transformations: []
                                                       )
        end

        it 'should respond with a status code 200' do
          expect(response.status).to eq(200)
        end
      end
    end

    context 'with invalid params' do
      before(:each) do
        post '/api/v1/images', params: {}
      end

      it 'should respond with a JSON giving the errors messages' do
        expect(response.headers['Content-Type']).to include 'application/json'

        expect(json_response[:errors]).to eq 'Wrong file format'
      end

      it 'should respond with a status code 400' do
        expect(response.status).to eq(400)
      end
    end
  end

  describe "#transform" do
    let(:image) { FactoryBot.create(:image) }

    context 'with valid params' do
      before(:each) do
        post "/api/v1/images/#{image.id}/transform", params: {specs: {format: 'png'}}
      end

      it 'should create a new image transformation' do

        latest_image = Image.order(created_at: :desc).first
        expect(json_response[:data][:attributes]).to eql(
                                                         id: latest_image.id,
                                                         file_url: latest_image.file_url,
                                                         transformations: []
                                                     )
      end

      it 'should respond with a status code 200' do
        expect(response.status).to eq(200)
      end
    end

    context 'with invalid params' do
      before(:each) do
        id = "randome_key"
        post "/api/v1/images/#{id}/transform", params: {specs: {format: 'png'}}
      end

      it "should return error message" do
        expect(json_response[:errors]).to eq("Original image not found")
      end

      it 'should respond with a status code 400' do
        expect(response.status).to eq(400)
      end
    end
  end
end
