ENV['JETS_TEST'] = '1'
ENV['JETS_ENV'] ||= 'test'
# Ensures aws api never called. Fixture home folder does not contain ~/.aws/credentails
ENV['HOME'] = 'spec/fixtures/home'
require 'byebug'
require 'fileutils'
require 'jets'

if Jets.env == 'production'
  abort('The Jets environment is running in production mode!')
end

Jets.boot

require 'jets/spec_helpers'
Dir[Jets.root.join('spec/factories/*.rb')].each { |f| require f }

module Helpers
  def payload(name)
    JSON.load(IO.read("spec/fixtures/payloads/#{name}.json"))
  end

  def json_response
    @json_response ||= JSON.parse(response.body, symbolize_names: true)
  end
end

RSpec.configure do |c|
  c.include Helpers
  c.include FactoryBot::Syntax::Methods
end
