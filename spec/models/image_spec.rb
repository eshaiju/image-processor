require 'spec_helper'

describe Image, type: :model do

  context 'when creating a new image' do
    before { @image = FactoryBot.create(:image) }

    it 'should validate the presence of the file' do
      expect(@image.file).not_to be_nil
    end
  end
end