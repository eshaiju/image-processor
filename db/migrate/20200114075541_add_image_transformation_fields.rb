class AddImageTransformationFields < ActiveRecord::Migration[6.0]
  def change
    add_column :images, :original_image_id, :string, index: true
    add_column :images, :specs, :jsonb, index: true
  end
end
