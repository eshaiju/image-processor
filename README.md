#### Image Processor API

Image Processor API is an image storage and processing service

## Getting Started

#### Install Ruby on Jets
```
gem install jets
```

#### Clone the repository
```
git clone git@gitlab.com:eshaiju/image-processor.git
```

#### Set the ENV variables
```
# .env
ACCESS_KEY_ID=
SECRET_ACCESS_KEY=

# .env.development
CARRIERWAVE_AWS_ACCESS_KEY_ID=
CARRIERWAVE_AWS_SECRET_ACCESS_KEY=
CARRIERWAVE_AWS_REGION=
CARRIERWAVE_S3_BUCKET=

RDS_NAME=
RDS_USERNAME=
RDS_PASSWORD=
RDS_URL=
RDS_PORT=

# .env.production
CARRIERWAVE_AWS_ACCESS_KEY_ID=
CARRIERWAVE_AWS_SECRET_ACCESS_KEY=
CARRIERWAVE_AWS_REGION=
CARRIERWAVE_S3_BUCKET=

RDS_NAME=
RDS_USERNAME=
RDS_PASSWORD=
RDS_URL=
RDS_PORT=
```
Start server after setup DB
```
JETS_ENV=development jets db:create db:migrate

jets server
```

Deploy to production
```
AWS_ACCESS_KEY_ID=YOUR_KEY_ID AWS_SECRET_ACCESS_KEY=YOUR_AWS_SECRET_ACCESS_KEY jets deploy production
```
This will generate a production endpoint.

![Image](public/endpoints.png)


API Postman collection

[Collection link](https://www.getpostman.com/collections/fb7968b912a3ae2d77c3)

[Link to exported collection](https://drive.google.com/file/d/1AeQaoWU0mOKxppjc7MKV0qUsxlWfpYRR/view?usp=sharing)
