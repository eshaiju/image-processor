class Image < ApplicationRecord
  has_many :transformations, foreign_key: 'original_image_id', class_name: 'Image'

  validates :file, presence: true

  mount_uploader :file, ImageUploader

  AVAILABLE_SPECS = [:format, :crop, :border, :resize, :rotate, :colorspace]
end
