class ImageSerializer < BaseSerializer
  attributes :id, :file_url, :transformations
end
