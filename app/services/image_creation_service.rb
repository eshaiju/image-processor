class ImageCreationService
  class InvalidFileFormat < StandardError;
  end

  attr_reader :params

  def initialize(params)
    @params = params
  end

  def call
    validate_params
    save_image
  end

  private

  def save_image
    image = Image.new

    if remote_url?
      image.remote_file_url = params[:file]
    else
      image.file = params[:file]
    end

    image.save!
    image
  rescue StandardError => e
    raise InvalidFileFormat, e.message
  end

  def validate_params
    raise InvalidFileFormat, 'Wrong file format' unless remote_url? || file?
  end

  def remote_url?
    return false unless params[:file].is_a?(String)

    uri = URI.parse(params[:file])
    %w[http https].include?(uri.scheme)
  rescue URI::BadURIError
    false
  rescue URI::InvalidURIError
    false
  end

  def file?
    params[:file].is_a?(File) ||
        params[:file].is_a?(ActionDispatch::Http::UploadedFile)
  end
end
