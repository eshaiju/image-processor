class ImageTransformationService
  class ImageNotFoundError < StandardError;
  end

  attr_reader :params, :original_image

  def initialize(params)
    @params = params
    @original_image = nil
  end

  def call
    find_original_image
    transform
  end

  private

  def find_original_image
    @original_image = Image.find_by(id: params[:id])
    raise ImageNotFoundError, 'Original image not found' unless @original_image.present?
  end

  def transform
    new_image = MiniMagick::Image.open(original_image.file_url)

    params[:specs].each do |spec, value|
      new_image.send(spec, value) if new_image.respond_to?(spec)
    end

    Image.create(
      original_image_id: original_image.id,
      specs: params[:specs],
      file: File.open(new_image.path)
      )
  end
end
