class Api::V1::ImagesController < ApplicationController
  def show
    image = Image.find_by(id: params[:id])

    if image
      render json: ImageSerializer.new(image), status: :ok
    else
      render json: {error: 'The file was not found.'}, status: :not_found
    end
  end

  def create
    image_creation_service = ImageCreationService.new(params)

    begin
      image = image_creation_service.call
      render json: ImageSerializer.new(image), status: :ok
    rescue ImageCreationService::InvalidFileFormat => e
      render json: {errors: e.message}, status: :bad_request
    end
  end

  def transform
    image_transformation_service = ImageTransformationService.new(transformation_params)

    begin
      image = image_transformation_service.call
      render json: ImageSerializer.new(image), status: :ok
    rescue ImageTransformationService::ImageNotFoundError => e
      render json: {errors: e.message}, status: :bad_request
    end
  end

  private

  def create_params
    params.permit(:file)
  end

  def transformation_params
    params.permit(:id, specs: Image::AVAILABLE_SPECS)
  end
end
